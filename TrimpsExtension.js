(function() {
    // Append jquery
    var jQuerySrc = document.createElement( 'script' );
    jQuerySrc.type = 'text/javascript';
    jQuerySrc.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js";
    document.head.appendChild( jQuerySrc );

    jQuerySrc.onload = function() {
        (function($){
            var formatTime = function(time) {
                var formattedTime = " (";
                var hours = Math.floor(time/3600);
                formattedTime += hours ? hours + "h" : "";
                var rem = time%3600;
                var minutes = Math.floor(rem/60);
                formattedTime += minutes ? minutes + "m" : (hours ? "00m" : "");
                var seconds = rem%60;
                formattedTime += seconds + "s";

                formattedTime += ")";
                return formattedTime;
            };

            var collector = {
                'food' : 'Farmer',
                'wood' : 'Lumberjack',
                'metal': 'Miner'
            };

            var getPerkModifier = function(perk) {
                return 1 + game.portal[perk].level * game.portal[perk].modifier;
            };

            var getMaxStorage = function(resource) {
                return game.resources[resource].max * getPerkModifier('Packrat');
            };

            var getSpeed = function (job) {
                return game.jobs[job].owned * Number(game.jobs[job].modifier) * getPerkModifier('Motivation');
            };

            var timeLeft = function(resource) {
                var resourceSpace = getMaxStorage(resource) - game.resources[resource].owned;
                var resourceSpeed = getSpeed(collector[resource]);

                // return the floor of the result to anticipate time
                return Math.floor(resourceSpace/resourceSpeed);
            };

            var calculateRemainingTime = function(resource) {
                var resourceTime = formatTime(timeLeft(resource));
                $('#'+resource+'Time').text(resourceTime);
            };

            var getStat = function(equipment) {
                var stat;
                if (equipment.blockNow) stat = "block";
                else stat = (typeof equipment.health !== 'undefined') ? "health" : "attack";
                return stat;
            };

            var getNextBonus = function (upgrade){
                var name = game.upgrades[upgrade].prestiges;
                var equipment = game.equipment[name];
                var stat = getStat(equipment);
                // WARNING: formula may change in future updates
                var toReturn = Math.round(equipment[stat] * Math.pow(1.19, ((equipment.prestige) * game.global.prestige[stat]) + 1));
                return toReturn;
            };

            var getCostToGoFromTo = function(fromLvl, toLvl, initialCost, costIncrease) {
                var cost = 0;
                var level;
                for (level = fromLvl; level < toLvl; level ++) {
                    cost += initialCost * Math.pow(costIncrease, level);
                }
                return cost;
            };

            var getLvlToUpgrade = function(upgrade) {
                var equipmentName = game.upgrades[upgrade].prestiges;
                if (!equipmentName) return 0;
                var equipment = game.equipment[equipmentName];
                var currentBonus = equipment[getStat(equipment)+'Calculated'] * equipment.level;
                var resource = (equipment.cost.metal) ? 'metal' : 'wood';
                var levelsNeeded = Math.ceil(currentBonus/getNextBonus(upgrade)) || 1;
                var resourcesNeeded = getCostToGoFromTo(0, levelsNeeded, getNextPrestigeCost(upgrade), equipment.cost[resource][1]);

                return (game.resources[resource].owned > resourcesNeeded) ? levelsNeeded : 0;
            };

            var createUpgradeTipsList = function() {
                var tipsDiv = document.createElement( 'div' );
                tipsDiv.id = 'tipsDiv';
                tipsDiv.title = 'How many levels needed after you purchase the upgrade to compensate it';
                $('#upgradesTitleDiv .row').append(tipsDiv);
                $('#tipsDiv').css('display', 'inline-block');
                $('#tipsDiv').css('font-size', '0.7em');
                $('#tipsDiv').css('width', '66.66%');
                $('#tipsDiv').css('text-align', 'left');
                $('#tipsDiv').append('<ul id="upgradeTipsList"></ul>');
            };

            var tipToUpgrade = function() {

                var upgradeTipsList = $('#upgradeTipsList');
                upgradeTipsList.children().remove();
                var upgrades = $('#upgradesHere').children();
                var i;
                for (i = 0; i < upgrades.length; i++) {
                    var upgrade = upgrades[i].id;
                    var upgradedItemLvl = getLvlToUpgrade(upgrade);
                    if (upgradedItemLvl) {
                        upgradeTipsList.append('<li style="float: left; margin-left: 25px;">' + upgrade + ': lvl ' +upgradedItemLvl +'</li>');
                    }
                }
            };

            $('#food span.title').append('<em id="foodTime"></em>');
            $('#wood span.title').append('<em id="woodTime"></em>');
            $('#metal span.title').append('<em id="metalTime"></em>');

            createUpgradeTipsList();

            setInterval(function() { calculateRemainingTime('metal'); }, 1000);
            setInterval(function() { calculateRemainingTime('wood'); }, 1000);
            setInterval(function() { calculateRemainingTime('food'); }, 1000);
            setInterval(function() { tipToUpgrade(); }, 1000);
        })(jQuery);
    };
    return;
})();
