# README #

Extends the functionality of the game Trimps (http://trimps.github.io/). What it does:

- Timer until your barn/shed/forge is full

- It tells you if you can upgrade an equipment's prestige so you can have the same attack/health/block after the upgrade

### How do I get set up? ###

For now, while in the game, open chrome's developer tools (F12) and copy the content of TrimpsExtension.js on the console.

If you're playing on kongregate, you have to select mainFrame(indexKong.html) on the console before you paste the code, as pointed out by reddit user PurePandemonium

### Contribution guidelines ###

Feel free to fork this and maybe make some pull request

### Who do I talk to? ###

felmilani[at]gmail[dot]com